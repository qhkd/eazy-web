import Login from 'containers/authentication/Login'
import Register from 'containers/authentication/Register'
import Reminder from 'containers/authentication/Reminder'

import Dashboard from 'containers/dashboard/Dashboard'
import HomePage from 'containers/dashboard/Home'
import OrderPage from 'containers/dashboard/Order'
import HistoryPage from 'containers/dashboard/History'
import StatisticPage from 'containers/dashboard/Statistic'
import ProductPage from 'containers/dashboard/Product'

import { Home, ShoppingCart, LocalLibrary, InsertChart, Camera } from '@material-ui/icons'

export const screenRoutes = [
  {
    path: '/login',
    screen: Login
  },
  {
    path: '/register',
    screen: Register
  },
  {
    path: '/reminder',
    screen: Reminder
  },
  {
    path: '/',
    screen: Dashboard
  }
]

export const dashboardRoutes = [
  {
    path: '/home',
    display: 'Home',
    icon: Home,
    content: HomePage
  },
  {
    path: '/order',
    display: 'Order',
    icon: ShoppingCart,
    content: OrderPage
  },
  {
    path: '/history',
    display: 'History',
    icon: LocalLibrary,
    content: HistoryPage
  },
  {
    path: '/statistic',
    display: 'Statistic',
    icon: InsertChart,
    content: StatisticPage
  },
  {
    path: '/product',
    display: 'Product',
    icon: Camera,
    content: ProductPage
  }
]
