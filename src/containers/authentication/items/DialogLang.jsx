import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import i18next from 'i18next'
import { translate } from 'react-i18next'
import { withStyles, Button } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import { HighlightOff } from '@material-ui/icons'

import { buttonBlue, buttonGreen, paddingHorizontal, paddingVertical, size } from 'assets/commonStyle'

class DialogLang extends React.Component {
  handleVieButton () {
    const lng = 'vi'
    i18next.changeLanguage(lng)
    localStorage.setItem('lng', lng)
    this.props.close()
  }

  handleEngButton () {
    const lng = 'en'
    i18next.changeLanguage(lng)
    localStorage.setItem('lng', lng)
    this.props.close()
  }

  render () {
    const { classes, t, window } = this.props
    return (
      <div className={classes.dialog}>
        <div className={classes.header}>
          <div className={classes.title}>{t('Choose language')}</div>
          <Button className={classes.buttonClose}
            onClick={() => this.props.close()}>
            <HighlightOff />
          </Button>
        </div>
        <div className={classes.content} style={{ maxHeight: (window.height - window.dialogLang) }}>
          <Button variant='outlined' className={classes.buttonVie}
            onClick={() => { this.handleVieButton() }}>
            <span className={classes.buttonLabel}>Tiếng Việt</span>
          </Button>
          <span className={classes.space} />
          <Button variant='outlined' className={classes.buttonEng}
            onClick={() => this.handleEngButton()}>
            <span className={classes.buttonLabel}>English</span>
          </Button>
        </div>
      </div>
    )
  }
}

DialogLang.propTypes = {
  classes: PropTypes.object.isRequired
}

const dialogStyle = (theme) => ({
  dialog: {

  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#009999',
    ...paddingHorizontal(20),
    ...paddingVertical(12)
  },
  title: {
    fontSize: 16,
    color: '#FFFFFF',
    fontFamily: 'Arial'
  },
  buttonClose: {
    color: '#E0E0E0',
    padding: 0,
    ...size(24)
  },
  content: {
    display: 'flex',
    padding: 20,
    borderBottom: '1px solid #E0E0E0',
    backgroundColor: '#FFFFFF',
    color: '#606060'
  },
  buttonVie: {
    ...buttonBlue,
    opacity: 0.7,
    borderRadius: 3
  },
  buttonEng: {
    ...buttonGreen,
    opacity: 1,
    borderRadius: 3
  },
  buttonLabel: {
    fontSize: 15
  },
  space: {
    ...size(12)
  }
})

export default compose(
  translate('translations'),
  withStyles(dialogStyle),
  withWindow
)(DialogLang)
