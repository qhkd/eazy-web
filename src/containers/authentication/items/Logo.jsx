import React from 'react'
import { withStyles } from '@material-ui/core'
import { AddShoppingCart } from '@material-ui/icons'

import { size } from 'assets/commonStyle'

const Logo = (props) => {
  const { classes } = props
  return (
    <div className={classes.logo}>
      <AddShoppingCart color='secondary' className={classes.icon} />
      <span style={{ color: 'forestgreen' }}>Eazy&nbsp;</span>
      <span style={{ color: 'deepskyblue' }}>Sho</span>
      <span style={{ color: 'orange' }}>p</span>
      <span style={{ color: 'orange' }}>p</span>
      <span style={{ color: 'deepskyblue' }}>ing</span>
    </div>
  )
}

const logoStyle = (theme) => ({
  logo: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    fontSize: 20,
    fontWeight: 700
  },
  icon: {
    ...size(28),
    marginRight: theme.spacing.unit
  }
})

export default withStyles(logoStyle)(Logo)
