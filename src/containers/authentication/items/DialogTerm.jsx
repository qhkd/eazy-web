import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withStyles, Button } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import { HighlightOff, Clear, Check } from '@material-ui/icons'

import { buttonGray, buttonGreen, paddingHorizontal, paddingVertical, size } from 'assets/commonStyle'

class DialogTerm extends React.Component {
  render () {
    const { classes, t, window } = this.props
    return (
      <div className={classes.dialog}>
        <div className={classes.header}>
          <div className={classes.title}>{t('Terms & Conditions')}</div>
          <Button className={classes.buttonClose}
            onClick={() => this.props.close()}>
            <HighlightOff />
          </Button>
        </div>
        <div className={classes.content} style={{ maxHeight: (window.height - window.dialogTerm) }}>
          <div className={classes.paragraph}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
          <div className={classes.paragraph}>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</div>
          <div className={classes.paragraph}>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>
          <div className={classes.paragraph}>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</div>
          <div className={classes.paragraph}>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</div>
        </div>
        <div className={classes.footer}>
          <Button variant='outlined' className={classes.buttonDecline}
            onClick={() => {
              this.props.handleDeclineButton()
              this.props.close()
            }}>
            <Clear className={classes.iconDecline} />{t('Decline')}
          </Button>
          <span className={classes.space} />
          <Button variant='outlined' className={classes.buttonAccept}
            onClick={() => {
              this.props.handleAcceptButton()
              this.props.close()
            }}>
            <Check className={classes.iconAccept} />{t('Perfect')}
          </Button>
        </div>
      </div>
    )
  }
}

DialogTerm.propTypes = {
  classes: PropTypes.object.isRequired
}

const dialogStyle = (theme) => ({
  dialog: {

  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#404040',
    ...paddingHorizontal(20),
    ...paddingVertical(12)
  },
  title: {
    fontSize: 16,
    color: '#FFFFFF',
    fontFamily: 'Arial'
  },
  buttonClose: {
    color: '#C0C0C0 ',
    padding: 0,
    ...size(24)
  },
  content: {
    overflow: 'auto',
    padding: 16,
    paddingBottom: 0,
    borderBottom: '1px solid #E0E0E0',
    backgroundColor: '#FFFFFF',
    color: '#606060'
  },
  paragraph: {
    marginBottom: 16,
    textAlign: 'justify'
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    backgroundColor: '#FFFFFF',
    ...paddingHorizontal(20),
    ...paddingVertical(8)
  },
  buttonDecline: {
    ...buttonGray,
    opacity: 0.6
  },
  iconDecline: {
    ...size(18)
  },
  buttonAccept: {
    ...buttonGreen,
    opacity: 1
  },
  iconAccept: {
    ...size(18),
    fontWeight: 800
  },
  space: {
    ...size(12)
  }
})

export default compose(
  translate('translations'),
  withStyles(dialogStyle),
  withWindow
)(DialogTerm)
