import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter, Link } from 'react-router-dom'
import { withStyles, FormControl, TextField, Button, FormControlLabel, Checkbox, Dialog } from '@material-ui/core'
import { PersonAdd, AspectRatio, ExitToApp } from '@material-ui/icons'
import Logo from 'containers/authentication/items/Logo'
import DialogTerm from 'containers/authentication/items/DialogTerm'

import registerStyle from 'assets/styles/authentication/registerStyle'

class Register extends React.Component {
  constructor (props) {
    super(props)
    this.handleOpenDialog = this.handleOpenDialog.bind(this)
    this.handleCloseDialog = this.handleCloseDialog.bind(this)
    this.handleAcceptButton = this.handleAcceptButton.bind(this)
    this.handleDeclineButton = this.handleDeclineButton.bind(this)
    this.handleChangeAccept = this.handleChangeAccept.bind(this)

    this.state = {
      openTerm: false,
      acceptTerm: false
    }
  }

  handleOpenDialog () {
    this.setState({
      openTerm: true
    })
  }

  handleCloseDialog () {
    this.setState({
      openTerm: false
    })
  }

  handleChangeAccept (event) {
    this.setState({
      acceptTerm: event.target.checked
    })
  }

  handleAcceptButton () {
    this.setState({
      acceptTerm: true
    })
  }

  handleDeclineButton () {
    this.setState({
      acceptTerm: false
    })
  }

  render () {
    const { classes, t } = this.props
    return (
      <div className={classes.register}>
        <div className={classes.bin}>
          <div className={classes.container}>
            <div className={classes.header}>
              <Logo />
              <div className={classes.headline}>
                Create New Account
              </div>
              <div className={classes.subheading}>
                Please add your details
              </div>
            </div>
            <div className={classes.content}>
              <FormControl className={classes.form}>
                <div className={classes.formRow} >
                  <TextField fullWidth label={t('Username')} />
                </div>
                <div className={classes.formRow} >
                  <TextField fullWidth type='password' label={t('Password')} />
                </div>
                <div className={classes.formRow} >
                  <TextField fullWidth type='password' label={t('Password Confirmation')} />
                </div>

                <div className={classes.formRow} >
                  <FormControlLabel
                    control={
                      <Checkbox style={{ color: 'green' }}
                        checked={this.state.acceptTerm}
                        onChange={this.handleChangeAccept} />
                    }
                    label={t('I agree to Terms and Conditions')} />
                </div>

                <div className={classes.formRow} >
                  <Button variant='raised' color='primary' className={classes.buttonPrimary}>
                    <PersonAdd className={classes.leftIcon} />
                    <span className={classes.labelIcon18}>{t('Register')}</span>
                  </Button>
                </div>
                <div className={classes.formRow} >
                  <Button variant='raised' color='primary' className={classes.buttonSecondary}
                    onClick={this.handleOpenDialog}>
                    <AspectRatio className={classes.leftIcon} />
                    <span className={classes.labelIcon16}>{t('Read Terms')}</span>
                  </Button>
                  <Dialog
                    open={this.state.openTerm}
                    onClose={this.handleCloseDialog}>
                    <DialogTerm close={this.handleCloseDialog}
                      handleDeclineButton={this.handleDeclineButton}
                      handleAcceptButton={this.handleAcceptButton} />
                  </Dialog>
                  <span className={classes.space} />
                  <Button variant='raised' color='primary' className={classes.buttonSecondary}
                    component={Link} to='/login'>
                    <ExitToApp className={classes.leftIcon} />
                    <span className={classes.labelIcon16}>{t('Login')}</span>
                  </Button>
                </div>
              </FormControl>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(registerStyle)
)(Register)
