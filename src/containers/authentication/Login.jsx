import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter, Link } from 'react-router-dom'
import { withStyles, FormControl, TextField, Button } from '@material-ui/core'
import { ExitToApp, ErrorOutline, AddCircleOutline } from '@material-ui/icons'
import Logo from 'containers/authentication/items/Logo'
import AuthService from 'services/AuthService'

import loginStyle from 'assets/styles/authentication/loginStyle'

class Login extends React.Component {
  constructor () {
    super()
    this.handleChange = this.handleChange.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.Auth = new AuthService()

    this.state = {
      username: '',
      password: ''
    }
  }

  componentWillMount () {
    if (this.Auth.loggedIn()) {
      this.props.history.replace('/home')
    }
  }

  handleChange (event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleFormSubmit (event) {
    event.preventDefault()

    this.Auth.login(this.state.username, this.state.password)
      .then(() => this.props.history.replace('/home'))
  }

  render () {
    const { classes, t } = this.props
    return (
      <div className={classes.login}>
        <div className={classes.bin}>
          <div className={classes.container}>
            <div className={classes.header}>
              <Logo />
              <div className={classes.headline}>
                Welcome to Your Dashboard
              </div>
              <div className={classes.subheading}>
                Please login
              </div>
            </div>
            <div className={classes.content}>
              <FormControl className={classes.form}>
                <div className={classes.formRow} >
                  <TextField fullWidth label={t('Username')}
                    onChange={this.handleChange} />
                </div>
                <div className={classes.formRow} >
                  <TextField fullWidth label={t('Password')} type='password'
                    onChange={this.handleChange} />
                </div>

                <div className={classes.formRow} >
                  <Button variant='raised' color='primary' className={classes.buttonPrimary}
                    component={Link} to='/home'
                    onClick={this.handleFormSubmit}>
                    <ExitToApp className={classes.leftIcon} />
                    <span className={classes.labelIcon18}>{t('Login')}</span>
                  </Button>
                </div>

                <div className={classes.formRow} >
                  <Button variant='raised' color='primary' className={classes.buttonSecondary}
                    component={Link} to='/reminder'>
                    <ErrorOutline className={classes.leftIcon} />
                    <span className={classes.labelIcon16}>{t('Forgot Password')}</span>
                  </Button>
                  <span className={classes.space} />
                  <Button variant='raised' color='primary' className={classes.buttonSecondary}
                    component={Link} to='/register'>
                    <AddCircleOutline className={classes.leftIcon} />
                    <span className={classes.labelIcon16}>{t('New Account')}</span>
                  </Button>
                </div>
              </FormControl>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(loginStyle)
)(Login)
