import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter, Link } from 'react-router-dom'
import { withStyles, FormControl, TextField, Button } from '@material-ui/core'
import { LocationOn, ExitToApp } from '@material-ui/icons'
import Logo from 'containers/authentication/items/Logo'

import reminderStyle from 'assets/styles/authentication/reminderStyle'

class Reminder extends React.Component {
  render () {
    const { classes, t } = this.props
    return (
      <div className={classes.reminder}>
        <div className={classes.bin}>
          <div className={classes.container}>
            <div className={classes.header}>
              <Logo />
              <div className={classes.headline}>
                Don’t worry, we’ve got your back
              </div>
              <div className={classes.subheading}>
                Please enter your username or email
              </div>
            </div>
            <div className={classes.content}>
              <FormControl className={classes.form}>
                <div className={classes.formRow} >
                  <TextField fullWidth label={t('Username')} />
                </div>

                <div className={classes.formRow} >
                  <Button variant='raised' color='primary' className={classes.buttonPrimary}>
                    <LocationOn className={classes.leftIcon} />
                    <span className={classes.labelIcon18}>{t('Password Reminder')}</span>
                  </Button>
                </div>

                <div className={classes.formRow} >
                  <div className={classes.buttonEmpty} />
                  <span className={classes.space} />
                  <Button variant='raised' color='primary' className={classes.buttonSecondary}
                    component={Link} to='/login'>
                    <ExitToApp className={classes.leftIcon} />
                    <span className={classes.labelIcon16}>{t('Login')}</span>
                  </Button>
                </div>
              </FormControl>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Reminder.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(reminderStyle)
)(Reminder)
