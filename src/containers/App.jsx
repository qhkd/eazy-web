import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withRouter, Switch, Route } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import 'containers/App.css'
import 'react-table/react-table.css'

import appStyle from 'assets/styles/appStyle'

import { screenRoutes } from 'routes/appRouter'

class App extends React.Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.app}>
        <Switch>
          {screenRoutes.map((route, index) =>
            <Route path={route.path} component={route.screen} key={index} />
          )}
        </Switch>
      </div >
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
  withStyles(appStyle)
)(App)
