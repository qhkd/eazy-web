import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import i18next from 'i18next'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles, Button } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import { HighlightOff } from '@material-ui/icons'
import ReactTable from 'react-table'

import incomingStyle from 'assets/styles/dashboard/incomingStyle'

class OrderIncoming extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      orders: [
        {
          time: Math.floor(Math.random() * 600) * 1000,
          address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội',
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Dầu gội đầu', price: 29000, amount: 2 },
            { name: 'Bột giặt', price: 39000, amount: 3 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ]
        },
        {
          time: Math.floor(Math.random() * 600) * 1000,
          address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội',
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Dầu gội đầu', price: 29000, amount: 2 },
            { name: 'Bột giặt', price: 39000, amount: 3 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ]
        },
        {
          time: Math.floor(Math.random() * 600) * 1000,
          address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội',
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Dầu gội đầu', price: 29000, amount: 2 },
            { name: 'Bột giặt', price: 39000, amount: 3 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ]
        },
        {
          time: Math.floor(Math.random() * 600) * 1000,
          address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội',
          products: [
            { name: 'Bột giặt', price: 39000, amount: 3 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ]
        },
        {
          time: Math.floor(Math.random() * 600) * 1000,
          address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội',
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Dầu gội đầu', price: 29000, amount: 2 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ]
        }
      ]
    }

    this.timer = 0
    this.countDown = this.countDown.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.finishTimer = this.finishTimer.bind(this)
  }

  componentDidMount () {
    this.startTimer()
  }

  componentWillUnmount () {
    this.finishTimer()
  }

  componentWillMount () {
    let orders = this.state.orders.slice()
    orders = orders.filter((order) => order.time > 0)
    orders.sort((orderA, orderB) => orderA.time - orderB.time)
    this.setState({ orders })
  }

  startTimer () {
    if (this.timer === 0) {
      this.timer = setInterval(this.countDown, 1000)
    }
  }

  finishTimer () {
    clearInterval(this.timer)
  }

  countDown () {
    let orders = this.state.orders.slice()
    orders.forEach((order) => { order.time = order.time - 1000 })
    orders = orders.filter((order) => order.time > 0)
    this.setState({ orders })
  }

  render () {
    const { classes, t, window } = this.props

    let Orders = this.state.orders.map((order, index) => (
      <Order props={{ classes, t }} order={order} key={index} />
    ))
    if (Orders.length === 0) {
      Orders = (
        <div className={classes.noDataText}>
          {t('You do not have any order')}
        </div>
      )
    }

    return (
      <div className={classes.incomingPage}
        style={{ height: (window.height - window.incoming) }}>
        <div className={classes.container}>
          {Orders}
        </div>
      </div >
    )
  }
}

const Order = ({ props, order }) => {
  const { classes, t } = props

  const toTextRemaining = (mils) => {
    const min = Math.floor(mils / 1000 / 60)
    const sec = Math.floor(mils / 1000) % 60
    const text = i18next.language === 'vi'
      ? `Còn ${min} phút ${sec} giây trước khi hết hạn`
      : `It's ${min} min ${sec} sec before expired`
    return text
  }

  let totalPrice = 0
  order.products.forEach((item) => {
    totalPrice += item.price * item.amount
  })
  return (
    <div className={classes.orderContainer}>
      <div className={classes.orderTop}>
        <div>
          <div className={classes.orderTime}>
            {toTextRemaining(order.time)}
          </div>
          <div className={classes.orderAddress}>
            <span>{t('Address')}</span>
            <span style={{ fontSize: 14, fontWeight: 600 }}>{': '}</span>
            <span>{order.address}</span>
          </div>
        </div>
        <Button className={classes.buttonClose}>
          <HighlightOff />
        </Button>
      </div>
      <ReactTable
        data={order.products}
        columns={[
          {
            Header: t('Product name'),
            accessor: 'name',
            Cell: (row) => (
              <div className={classes.cellName}>{row.value}</div>
            )
          },
          {
            Header: t('Price'),
            accessor: 'price',
            Cell: (row) => (
              <div className={classes.cellPrice}>{row.value}</div>
            ),
            Footer: (
              <div className={classes.footerPrice}>
                <span>{t('Total')}</span>
                <span>{totalPrice}</span>
              </div>
            )
          },
          {
            Header: t('Amount'),
            accessor: 'amount',
            Cell: (row) => (
              <div className={classes.cellAmount}>{row.value}</div>
            )
          }
        ]}
        defaultPageSize={order.products.length}
        showPagination={false}
        className='-highlight'
        multiSort={false}
        getTheadThProps={() => {
          return {
            style: {
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: 32,
              fontSize: 16,
              backgroundColor: '#fff'
            }
          }
        }}
      />
      <div className={classes.orderBottom}>
        <Button variant='outlined' className={classes.buttonAccept}>
          <span className={classes.buttonLabel}>{t('Accept')}</span>
        </Button>
      </div>
    </div>
  )
}

OrderIncoming.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(incomingStyle),
  withWindow
)(OrderIncoming)
