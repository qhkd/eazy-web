import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import { } from '@material-ui/icons'
import ReactTable from 'react-table'

import historyStyle from 'assets/styles/dashboard/historyStyle'

class History extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      data: this.makeData(128)
    }
  }

  newPerson () {
    const statusChance = Math.random()
    var names = ['Rock', 'Paper', 'Scissor']
    return {
      firstName: names[Math.floor(Math.random() * names.length)],
      lastName: names[Math.floor(Math.random() * names.length)],
      age: Math.floor(Math.random() * 50),
      visits: Math.floor(Math.random() * 100),
      progress: Math.floor(Math.random() * 100),
      status: statusChance > 0.66 ? 'relationship'
        : statusChance > 0.33 ? 'complicated' : 'single'
    }
  }
  makeChildren (age) {
    let num = Math.floor(age / 10) - 1
    const arr = []
    for (let i = 0; i < num; i++) arr.push(i)
    return arr.map(() => ({ ...this.newPerson() }))
  }
  makeData (num) {
    const arr = []
    for (let i = 0; i < num; i++) arr.push(i)
    return arr.map(() => {
      const person = this.newPerson()
      return { ...person, children: this.makeChildren(person.age) }
    })
  }

  render () {
    const { classes, window } = this.props
    const { data } = this.state
    return (
      <div className={classes.historyPage} >
        <ReactTable style={{ maxHeight: (window.height - window.history) }}
          data={data}
          columns={[
            {
              Header: 'First Name',
              accessor: 'firstName',
              filterable: true
            }, {
              Header: 'Last Name',
              accessor: 'lastName',
              filterable: true
            }, {
              Header: 'Profile Progress',
              accessor: 'progress',
              Cell: row => (
                <div style={{ width: '100%', height: '100%', backgroundColor: '#dadada', borderRadius: '2px' }}>
                  <div style={{
                    width: `${row.value}%`,
                    height: '100%',
                    backgroundColor: row.value > 66 ? '#85cc00'
                      : row.value > 33 ? '#ffbf00' : '#ff2e00',
                    borderRadius: '2px',
                    transition: 'all .2s ease-out'
                  }} />
                </div>
              )
            }, {
              Header: 'Status',
              accessor: 'status',
              Cell: row => (
                <span>
                  <span style={{
                    color: row.value === 'relationship' ? '#ff2e00'
                      : row.value === 'complicated' ? '#ffbf00' : '#57d500',
                    transition: 'all .3s ease'
                  }} >
                    &#x25cf;
                  </span>
                  {row.value === 'relationship' ? ` In a relationship`
                    : row.value === 'complicated' ? ` It's complicated` : ` Single`}
                </span>
              )
            }
          ]}
          defaultPageSize={10}
          className='-striped -highlight'
          multiSort={false}
          getTheadThProps={() => {
            return {
              style: {
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: 40,
                fontSize: 16,
                backgroundColor: '#fff'
              }
            }
          }}
          defaultFilterMethod={(filter, row, column) => {
            const id = filter.pivotId || filter.id
            return row[id] === undefined ? true
              : String(row[id]).toUpperCase().indexOf(filter.value.toUpperCase()) > -1
          }}
          SubComponent={(row) => {
            const size = row.original.children.length
            if (size > 0) {
              return <ReactTable
                style={{ marginLeft: 35, marginBottom: 8, marginRight: -1 }}
                data={row.original.children}
                showPagination={false}
                columns={[
                  {
                    Header: 'First Name',
                    accessor: 'firstName'
                  }, {
                    Header: 'Last Name',
                    accessor: 'lastName'
                  }, {
                    Header: 'Age',
                    accessor: 'age'
                  }, {
                    Header: 'Visits',
                    accessor: 'visits'
                  }
                ]}
                defaultPageSize={size}
                noDataText={null}
              />
            }
          }}
        />
      </div>
    )
  }
}

History.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(historyStyle),
  withWindow
)(History)
