import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter, Redirect, Link } from 'react-router-dom'
import { withStyles, Paper, Tabs, Tab } from '@material-ui/core'
import { GetApp, Directions } from '@material-ui/icons'
import OrderIncoming from 'containers/dashboard/Incoming'
import OrderProcessing from 'containers/dashboard/Ongoing'

class Order extends React.Component {
  render () {
    const { classes, t } = this.props

    const pathname = this.props.location.pathname
    let selected
    if (pathname.startsWith('/order/incoming')) selected = 'incoming'
    else if (pathname.startsWith('/order/processing')) selected = 'processing'
    else if (pathname.startsWith('/order')) {
      if (1) {
        return <Redirect to='/order/incoming' />
      } else {
        if (Math.random() > 0.5) { return <Redirect to='/order/incoming' /> } else { return <Redirect to='/order/processing' /> }
      }
    }

    const myTabclasses = {
      labelIcon: classes.tab_labelIcon,
      wrapper: classes.tab_wrapper,
      label: classes.tab_label
    }

    const Content = pathname.startsWith('/order/incoming') ? OrderIncoming
      : pathname.startsWith('/order/processing') ? OrderProcessing : null

    return (
      <div className={classes.orderPage}>
        <Paper>
          <Tabs value={selected} indicatorColor='primary' textColor='primary'>
            <Tab icon={<GetApp />} label={t('Incoming')} value={'incoming'}
              component={Link} to='/order/incoming'
              classes={myTabclasses} />
            <Tab icon={<Directions />} label={t('Processing')} value={'processing'}
              component={Link} to='/order/processing'
              classes={myTabclasses} />
          </Tabs>
        </Paper>
        <Content />
      </div >
    )
  }
}

Order.propTypes = {
  classes: PropTypes.object.isRequired
}

const orderStyle = (theme) => ({
  orderPage: {

  },
  tab_labelIcon: {
    height: 48
  },
  tab_wrapper: {
    flexDirection: 'row',
    paddingLeft: 12
  },
  tab_label: {
    fontWeight: 600
  }
})

export default compose(
  translate('translations'),
  withRouter,
  withStyles(orderStyle)
)(Order)
