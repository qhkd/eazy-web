import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { } from '@material-ui/icons'

import productStyle from 'assets/styles/dashboard/productStyle'

class Product extends React.Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.productPage}>
        Product
      </div>
    )
  }
}

Product.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(productStyle)
)(Product)
