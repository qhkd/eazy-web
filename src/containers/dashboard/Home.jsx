import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { } from '@material-ui/icons'

import homeStyle from 'assets/styles/dashboard/homeStyle'

class Home extends React.Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.homePage}>
        Home
      </div>
    )
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(homeStyle)
)(Home)
