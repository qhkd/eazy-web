import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withRouter, Switch, Route } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import Sidebar from 'components/Sidebar'
import Header from 'components/Header'
import Footer from 'components/Footer'
import AuthService from 'services/AuthService'

import dashboardStyle from 'assets/styles/dashboard/dashboardStyle'
import { backgroundScreen } from 'assets/commonStyle'

import { dashboardRoutes } from 'routes/appRouter'

class Dashboard extends React.Component {
  constructor (props) {
    super(props)
    this.handleChangeSidebarOpen = this.handleChangeSidebarOpen.bind(this)
    this.Auth = new AuthService()

    this.state = {
      sidebar: {
        open: true
      }
    }
  }

  componentWillMount () {
    if (!this.Auth.loggedIn()) {
      this.props.history.replace('/login')
    }
  }

  handleChangeSidebarOpen () {
    this.setState({
      sidebar: {
        open: !this.state.sidebar.open
      }
    })
  }

  render () {
    const { classes, window } = this.props
    return (
      <Switch>
        {dashboardRoutes.map((route, index) => {
          const Content = route.content
          return <Route path={route.path} key={index} render={() => (
            <div className={classes.dashboard}>
              <Sidebar status={this.state.sidebar} />
              <div className={classes.container}>
                <Header onChangeSidebarOpen={this.handleChangeSidebarOpen} />
                <div className={classes.content}
                  style={{
                    minHeight: (window.height - window.dashboard),
                    ...backgroundScreen
                  }}>
                  <Content />
                </div>
                <Footer />
              </div>
            </div>
          )} />
        })}
      </Switch>
    )
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withRouter,
  withStyles(dashboardStyle),
  withWindow
)(Dashboard)
