import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core'
import { } from '@material-ui/icons'

import statisticStyle from 'assets/styles/dashboard/statisticStyle'

class Statistic extends React.Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.statisticPage}>
        Statistic
      </div>
    )
  }
}

Statistic.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(statisticStyle)
)(Statistic)
