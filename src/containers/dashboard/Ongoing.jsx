import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import i18next from 'i18next'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles, Button } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import { } from '@material-ui/icons'
import ReactTable from 'react-table'

import ongoingStyle from 'assets/styles/dashboard/ongoingStyle'

class OrderOngoing extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      orders: [
        {
          customer: {
            name: 'Nguyễn Văn A',
            phone: '0915219523',
            address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội'
          },
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Dầu gội đầu', price: 29000, amount: 2 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ],
          status: 1,
          note: 'Ship trước 7h tối, ship đi thì gọi báo trước 10 phút'
        }, {
          customer: {
            name: 'Nguyễn Văn A',
            phone: '0915219523',
            address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội'
          },
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ],
          status: 2,
          note: ''
        }, {
          customer: {
            name: 'Nguyễn Văn A',
            phone: '0915219523',
            address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội'
          },
          products: [
            { name: 'Kem đánh răng', price: 19000, amount: 1 },
            { name: 'Dầu gội đầu', price: 29000, amount: 2 },
            { name: 'Bột giặt', price: 39000, amount: 3 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ],
          status: 2,
          note: 'Ship trước 7h tối, ship đi thì gọi báo trước 10 phút'
        }, {
          customer: {
            name: 'Nguyễn Văn A',
            phone: '0915219523',
            address: 'Số 100 - Ngõ 123 - Xuân Thủy - Cầu Giấy - Hà Nội'
          },
          products: [
            { name: 'Bột giặt', price: 39000, amount: 3 },
            { name: 'Sữa chua', price: 49000, amount: 4 }
          ],
          status: 1,
          note: 'Ship trước 7h tối, ship đi thì gọi báo trước 10 phút'
        }
      ]
    }
  }

  render () {
    const { classes, t, window } = this.props

    let Orders = this.state.orders.map((order, index) => (
      <Order props={{ classes, t }} order={order} key={index} />
    ))
    if (Orders.length === 0) {
      Orders = (
        <div className={classes.noDataText}>
          {t('You do not have any order')}
        </div>
      )
    }

    return (
      <div className={classes.ongoingPage}
        style={{ height: (window.height - window.ongoing) }}>
        <div className={classes.container}>
          {Orders}
        </div>
      </div>
    )
  }
}

const Order = ({ props, order }) => {
  const { classes, t } = props

  let totalPrice = 0
  order.products.forEach((item) => {
    totalPrice += item.price * item.amount
  })

  const labelWidth = i18next.language === 'vi' ? 105 : 70

  const textStatus = order.status === 1 ? 'Waiting to delivery' : 'Being delivered'
  const coloStatus = order.status === 1 ? '#004C99' : '#4C9900'

  const textButton = order.status === 1 ? 'Deliver' : 'Close'
  const typeButton = order.status === 1 ? classes.buttonDelivery : classes.buttonClose

  return (
    <div className={classes.orderContainer}>
      <div className={classes.orderTop}>
        <div className={classes.customerInfo}>
          <div className={classes.lineTop}>
            <span style={{ minWidth: labelWidth }}>{t('Buyer')}</span>
            <span>{order.customer.name}</span>
          </div>
          <div className={classes.lineTop}>
            <span style={{ minWidth: labelWidth }}>{t('Phone')}</span>
            <span>{order.customer.phone}</span>
          </div>
          <div className={classes.lineTop}>
            <span style={{ minWidth: labelWidth }}>{t('Address')}</span>
            <span>{order.customer.address}</span>
          </div>
          <div className={classes.lineTop}>
            <span style={{ minWidth: labelWidth }}>{t('Note')}</span>
            <span>{order.note}</span>
          </div>
        </div>
        <div className={classes.orderStatus} style={{ color: coloStatus }}>
          {t(textStatus)}
        </div>
      </div>
      <ReactTable
        data={order.products}
        columns={[
          {
            Header: t('Product name'),
            accessor: 'name',
            Cell: (row) => (
              <div className={classes.cellName}>{row.value}</div>
            )
          },
          {
            Header: t('Price'),
            accessor: 'price',
            Cell: (row) => (
              <div className={classes.cellPrice}>{row.value}</div>
            ),
            Footer: (
              <div className={classes.footerPrice}>
                <span>{t('Total')}</span>
                <span>{totalPrice}</span>
              </div>
            )
          },
          {
            Header: t('Amount'),
            accessor: 'amount',
            Cell: (row) => (
              <div className={classes.cellAmount}>{row.value}</div>
            )
          }
        ]}
        defaultPageSize={order.products.length}
        showPagination={false}
        className='-highlight'
        multiSort={false}
        getTheadThProps={() => {
          return {
            style: {
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              height: 32,
              fontSize: 16,
              backgroundColor: '#fff'
            }
          }
        }}
      />
      <div className={classes.orderBottom}>
        <Button variant='outlined' className={classes.buttonCancel}>
          <span className={classes.buttonLabel}>{t('Cancel')}</span>
        </Button>
        <span className={classes.space} />
        <Button variant='outlined' className={typeButton}>
          <span className={classes.buttonLabel}>{t(textButton)}</span>
        </Button>
      </div>
    </div>
  )
}

OrderOngoing.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(ongoingStyle),
  withWindow
)(OrderOngoing)
