class AuthService {
  login (username, password) {
    this.setToken({
      user: {
        username,
        password
      }
    })
    return Promise.resolve()
  }

  loggedIn () {
    const token = this.getToken()
    return !!token && !this.isTokenExpired(token)
  }

  isTokenExpired (token) {
    return !token
  }

  setToken (token) {
    localStorage.setItem('id_token', token)
  }

  getToken () {
    return localStorage.getItem('id_token')
  }

  logout () {
    localStorage.removeItem('id_token')
  }
}

export default AuthService
