import React from 'react'

// This function takes a component...
export const withWindow = (Component) => {
  // ...and returns another component...
  return class extends React.Component {
    constructor (props) {
      super(props)
      this.updateDimensions = this.updateDimensions.bind(this)

      const page = {
        header: 64,
        footer: 64,
        paddingTop: 8,
        paddingBottom: 8,
        tabBar: 48,
        divider: 16
      }

      const dialog = {
        header: 48,
        footer: 52,
        marginTop: 50,
        marginBottom: 50
      }

      this.state = {
        window: {
          width: window.innerWidth,
          height: window.innerHeight,
          sidebar: 0,
          dialogTerm: dialog.header + dialog.footer + dialog.marginTop + dialog.marginBottom,
          dialogLang: dialog.header + dialog.footer + dialog.marginTop + dialog.marginBottom,
          dashboard: page.header + page.footer,
          history: page.header + page.footer + page.paddingTop + page.paddingBottom,
          incoming: page.header + page.footer + page.paddingTop + page.paddingBottom + page.tabBar + page.divider,
          ongoing: page.header + page.footer + page.paddingTop + page.paddingBottom + page.tabBar + page.divider
        }
      }
    }

    componentDidMount () {
      // ... that takes care of the subscription...
      window.addEventListener('resize', this.updateDimensions)
    }

    componentWillUnmount () {
      window.removeEventListener('resize', this.updateDimensions)
    }

    updateDimensions () {
      this.setState({
        window: Object.assign({}, this.state.window, {
          width: window.innerWidth,
          height: window.innerHeight
        })
      })
    }

    render () {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return <Component window={this.state.window
      } {...this.props} />
    }
  }
}
