import React from 'react'
import PropTypes from 'prop-types'
import { withStyles, List, ListItem } from '@material-ui/core'

import footerStyle from 'assets/styles/components/footerStyle'

class Footer extends React.Component {
  render () {
    const { classes } = this.props
    return (
      <footer className={classes.footer}>
        <div className={classes.container}>
          <div className={classes.left}>
            <List className={classes.list}>
              <ListItem className={classes.listItem}>
                <a href='' className={classes.block}>Home</a>
              </ListItem>
              <ListItem className={classes.listItem}>
                <a href='' className={classes.block}>Company</a>
              </ListItem>
              <ListItem className={classes.listItem}>
                <a href='' className={classes.block}>Portfolio</a>
              </ListItem>
              <ListItem className={classes.listItem}>
                <a href='' className={classes.block}>Blog</a>
              </ListItem>
            </List>
          </div>
          <div className={classes.right} />
        </div>
      </footer>
    )
  }
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(footerStyle)(Footer)
