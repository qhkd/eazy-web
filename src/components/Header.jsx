import React from 'react'
import PropTypes from 'prop-types'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import { withStyles, IconButton, Button } from '@material-ui/core'
import { Menu, PresentToAll } from '@material-ui/icons'
import AuthService from 'services/AuthService'

import headerStyle from 'assets/styles/components/headerStyle'

class Header extends React.Component {
  constructor () {
    super()
    this.handleLogout = this.handleLogout.bind(this)
    this.Auth = new AuthService()
  }

  render () {
    const { classes, t } = this.props
    return (
      <header className={classes.header}>
        <div className={classes.container}>
          <div>
            <IconButton onClick={() => this.props.onChangeSidebarOpen()}>
              <Menu />
            </IconButton>
            <Button variant='outlined' className={classes.notificationButton}>
              <span className={classes.buttonLabel}>{t('Notifications')}</span>
              <span className={classes.notificationNumber}>12</span>
            </Button>
          </div>
          <div>
            <Button variant='outlined' className={classes.profileButton}>
              <span className={classes.buttonLabel}>{t('Profile')}</span>
            </Button>
            <IconButton className={classes.logoutIcon} onClick={this.handleLogout}>
              <PresentToAll />
            </IconButton>
          </div>
        </div>
      </header>
    )
  }

  handleLogout () {
    this.Auth.logout()
    this.props.history.replace('/login')
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  translate('translations'),
  withRouter,
  withStyles(headerStyle)
)(Header)
