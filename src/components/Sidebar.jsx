import React from 'react'
import PropTypes from 'prop-types'
import classname from 'classnames'
import { compose } from 'recompose'
import { translate } from 'react-i18next'
import { withRouter, Link } from 'react-router-dom'
import { withStyles, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText, Dialog } from '@material-ui/core'
import { withWindow } from 'services/WindowDimension'
import { AddShoppingCart, Settings } from '@material-ui/icons'
import { dashboardRoutes } from 'routes/appRouter'
import DialogLang from 'containers/authentication/items/DialogLang'

import sidebarStyle from 'assets/styles/components/sidebarStyle'

class Sidebar extends React.Component {
  constructor (props) {
    super(props)
    this.handleOpenDialog = this.handleOpenDialog.bind(this)
    this.handleCloseDialog = this.handleCloseDialog.bind(this)

    this.state = {
      openSetting: false
    }
  }

  handleOpenDialog () {
    this.setState({
      openSetting: true
    })
  }

  handleCloseDialog () {
    this.setState({
      openSetting: false
    })
  }

  render () {
    const { classes, t, window } = this.props
    const { status } = this.props

    const dashboardItems = dashboardRoutes.map((route, index) => {
      const isActive = this.props.location.pathname.startsWith(route.path)
      const listItemclasses = classname({ [' ' + classes.active]: isActive })
      const blackFontClasses = classname({ [' ' + classes.blackFont]: isActive })
      return (
        <ListItem button
          className={classes.listItem + listItemclasses}
          component={Link} to={route.path} key={index}>
          <ListItemIcon className={blackFontClasses}>
            <route.icon />
          </ListItemIcon>
          <ListItemText primary={t(route.display)} />
        </ListItem >
      )
    })

    return (
      <div className={classes.sidebar}>
        <Drawer style={{ height: (window.height - window.sidebar) }}
          variant='permanent' open={status.open}
          classes={{ paper: classname(classes.drawerPaper, !status.open && classes.drawerPaperClose) }}>
          <List>
            <ListItem button className={classes.listItem}>
              <ListItemIcon>
                <AddShoppingCart />
              </ListItemIcon>
              <ListItemText primary='Eazy Shopping' />
            </ListItem >
          </List>
          <Divider />
          <List>
            {dashboardItems}
          </List>
          <Divider />
          <List>
            <ListItem button className={classes.listItem}
              onClick={this.handleOpenDialog}>
              <ListItemIcon>
                <Settings />
              </ListItemIcon>
              <ListItemText primary={t('Setting')} />
            </ListItem>
            <Dialog
              open={this.state.openSetting}
              onClose={this.handleCloseDialog}>
              <DialogLang close={this.handleCloseDialog} />
            </Dialog>
          </List>
        </Drawer>
      </div>
    )
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
}

export default compose(
  translate('translations'),
  withRouter,
  withStyles(sidebarStyle),
  withWindow
)(Sidebar)
