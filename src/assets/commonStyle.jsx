const normalButton = {
  fontSize: 14,
  fontWeight: 600,
  opacity: 0.8,
  textTransform: 'none',
  boxShadow: 'none'
}

export const backgroundScreen = {
  backgroundColor: '#F0F0F0'
}

export const backgroundBlue = {
  background: 'linear-gradient(45deg, #66B2FF, #B266FF)'
}

export const buttonBlue = {
  backgroundColor: '#CCE5FF',
  color: '#004C99',
  ...normalButton,
  '&:hover': {
    backgroundColor: '#99CCFF'
  }
}

export const backgroundGreen = {
  background: 'linear-gradient(45deg, #66CC00, #00CCCC)'
}

export const buttonGreen = {
  backgroundColor: '#E5FFCC',
  color: '#4C9900',
  ...normalButton,
  '&:hover': {
    backgroundColor: '#CCFF99'
  }
}

export const backgroundOrange = {
  background: 'linear-gradient(45deg, #FFCC99, #FF9999)'
}

export const buttonOrange = {
  backgroundColor: '#FFE5CC',
  color: '#994C00',
  ...normalButton,
  '&:hover': {
    backgroundColor: '#FFCC99'
  }
}

export const buttonGray = {
  backgroundColor: '#E0E0E0',
  color: '#202020',
  ...normalButton,
  '&:hover': {
    backgroundColor: '#C0C0C0'
  }
}

export const size = (unit) => ({
  maxWidth: unit,
  maxHeight: unit,
  minWidth: unit,
  minHeight: unit
})

export const paddingVertical = (unit) => ({
  paddingTop: unit,
  paddingBottom: unit
})

export const paddingHorizontal = (unit) => ({
  paddingLeft: unit,
  paddingRight: unit
})

export const marginVertical = (unit) => ({
  marginTop: unit,
  marginBottom: unit
})

export const marginHorizontal = (unit) => ({
  marginLeft: unit,
  marginRight: unit
})

export const transition = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen
  })
})
