const defaultFont = {
  fontFamily: 'Arial, sans-serif',
  fontSize: 14,
  fontWeight: 400
}

const screenWidth = 700

const appStyle = (theme) => ({
  app: {
    minWidth: screenWidth,
    height: '100%',
    minHeight: '100%!important',
    ...defaultFont
  }
})

export default appStyle
