import { paddingHorizontal, paddingVertical } from 'assets/commonStyle'

const dashboardStyle = (theme) => ({
  dashboard: {
    display: 'flex'
  },
  container: {
    flex: 1
  },
  content: {
    ...paddingHorizontal(12),
    ...paddingVertical(8)
  }
})

export default dashboardStyle
