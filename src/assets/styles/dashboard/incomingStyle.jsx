import { buttonOrange, size } from 'assets/commonStyle'

const incomingStyle = (theme) => ({
  incomingPage: {
    marginTop: 16,
    backgroundColor: '#FFFFFF',
    boxShadow: '0 0 5px 0 #A0A0A0',
    overflow: 'auto'
  },
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  noDataText: {
    margin: 12,
    fontSize: 16,
    fontWeight: 600,
    color: '#808080'
  },
  orderContainer: {
    margin: 12,
    marginBottom: 0,
    borderBottom: '1px solid #808080'
  },
  orderTop: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  orderTime: {
    marginBottom: 8,
    fontSize: 16,
    fontWeight: 600,
    color: '#FF8000'
  },
  orderAddress: {
    marginBottom: 8,
    fontSize: 16
  },
  buttonClose: {
    color: '#A0A0A0',
    marginRight: 16,
    padding: 0,
    ...size(24)
  },
  cellName: {
    textAlign: 'start'
  },
  cellPrice: {
    textAlign: 'end'
  },
  footerPrice: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'start'
  },
  cellAmount: {
    textAlign: 'center'
  },
  orderBottom: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  buttonAccept: {
    ...buttonOrange,
    marginTop: 8,
    marginRight: 16,
    marginBottom: 12
  },
  buttonLabel: {
    fontSize: 16
  }
})

export default incomingStyle
