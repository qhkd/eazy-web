import { buttonGray, buttonBlue, buttonGreen, size } from 'assets/commonStyle'

const ongoingStyle = (theme) => ({
  ongoingPage: {
    marginTop: 16,
    backgroundColor: '#FFFFFF',
    boxShadow: '0 0 5px 0 #A0A0A0',
    overflow: 'auto'
  },
  container: {
    display: 'flex',
    flexDirection: 'column'
  },
  noDataText: {
    margin: 12,
    fontSize: 16,
    fontWeight: 600,
    color: '#808080'
  },
  orderContainer: {
    margin: 12,
    marginBottom: 0,
    borderBottom: '1px solid #808080'
  },
  orderTop: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingTop: 8,
    paddingLeft: 5,
    border: '1px solid #E0E0E0',
    borderBottomWidth: 0
  },
  lineTop: {
    display: 'flex',
    marginBottom: 8,
    fontSize: 16
  },
  orderStatus: {
    marginBottom: 8,
    marginRight: 16,
    fontSize: 16,
    fontWeight: 600
  },
  cellName: {
    textAlign: 'start'
  },
  cellPrice: {
    textAlign: 'end'
  },
  footerPrice: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'start'
  },
  cellAmount: {
    textAlign: 'center'
  },
  orderBottom: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  buttonDelivery: {
    ...buttonBlue,
    opacity: 0.7,
    width: 130,
    marginTop: 8,
    marginRight: 16,
    marginBottom: 12
  },
  buttonClose: {
    ...buttonGreen,
    opacity: 1,
    width: 130,
    marginTop: 8,
    marginRight: 16,
    marginBottom: 12
  },
  buttonCancel: {
    ...buttonGray,
    opacity: 0.6,
    marginTop: 8,
    marginBottom: 12
  },
  buttonLabel: {
    fontSize: 16
  },
  space: {
    ...size(12)
  }
})

export default ongoingStyle
