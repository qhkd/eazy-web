import { backgroundScreen, paddingHorizontal } from 'assets/commonStyle'

const footerStyle = (theme) => ({
  footer: {
    display: 'flex',
    alignItems: 'center',
    height: 64,
    ...backgroundScreen
  },
  container: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  left: {
    whiteSpace: 'nowrap'
  },
  right: {

  },
  list: {
    margin: 0,
    padding: 0
  },
  listItem: {
    display: 'inline-block',
    width: 'auto',
    ...paddingHorizontal(12)
  },
  block: {
    textDecoration: 'none',
    textTransform: 'uppercase',
    fontSize: 12,
    fontWeight: 600
  }
})

export default footerStyle
