import { transition, paddingHorizontal } from 'assets/commonStyle'

const sidebarStyle = (theme) => ({
  sidebar: {
    zIndex: theme.zIndex.appBar + 1,
    boxShadow: '0 0 5px 0 #A0A0A0'
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    overflowX: 'hidden',
    ...transition(theme),
    height: '100%',
    width: 220,
    border: 0
  },
  drawerPaperClose: {
    position: 'relative',
    ...transition(theme),
    height: '100%',
    width: 56,
    border: 0
  },
  listItem: {
    ...paddingHorizontal(16)
  },
  active: {
    backgroundColor: '#D0E0E0'
  },
  blackFont: {
    color: '#606060'
  }
})

export default sidebarStyle
