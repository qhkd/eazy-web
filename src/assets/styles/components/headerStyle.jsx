import { backgroundScreen, paddingHorizontal } from 'assets/commonStyle'

const headerStyle = (theme) => ({
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 64,
    ...backgroundScreen
  },
  container: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    ...paddingHorizontal(12)
  },
  notificationButton: {
    marginLeft: 8
  },
  notificationNumber: {
    fontSize: 13,
    fontWeight: 600,
    marginLeft: 8,
    ...paddingHorizontal(6),
    borderRadius: 100,
    color: '#FFFFFF',
    backgroundColor: '#4C9900'
  },
  profileButton: {
    marginRight: 8
  },
  buttonLabel: {
    fontSize: 16,
    textTransform: 'none'
  },
  logoutIcon: {
    transform: 'rotate(90deg)'
  }
})

export default headerStyle
