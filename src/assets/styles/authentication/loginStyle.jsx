import { backgroundBlue, buttonBlue, buttonGray, paddingVertical, size } from 'assets/commonStyle'

const loginStyle = (theme) => ({
  login: {
    minHeight: '100%',
    ...backgroundBlue,
    display: 'flex',
    justifyContent: 'center'
  },
  bin: {
    width: 800,
    backgroundColor: '#FFFFFF'
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  header: {
    textAlign: 'center',
    paddingTop: 70,
    paddingBottom: 30
  },
  headline: {
    fontSize: 30,
    fontWeight: 700,
    color: '#202020',
    marginTop: 50,
    marginBottom: 20
  },
  subheading: {
    fontSize: 22,
    fontWeight: 400,
    color: '#606060'
  },
  content: {
    width: 400,
    minWidth: 400,
    padding: 10,
    paddingBottom: 70
  },
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  formRow: {
    display: 'flex',
    justifyContent: 'center',
    ...paddingVertical(8)
  },
  buttonPrimary: {
    ...buttonBlue,
    width: '100%',
    borderRadius: 100,
    padding: 12
  },
  buttonSecondary: {
    ...buttonGray,
    width: '100%',
    borderRadius: 100,
    padding: 6,
    fontSize: 12
  },
  leftIcon: {
    marginRight: 8,
    ...size(18)
  },
  labelIcon18: {
    height: 18
  },
  labelIcon16: {
    height: 16
  },
  space: {
    ...size(12)
  }
})

export default loginStyle
