import { combineReducers } from 'redux'
import * as Action from 'redux/actions/actionTypes'

const getToken = (state = null, action) => {
  switch (action.type) {
    case Action.RECEIVE_TOKEN:
    case Action.REFRESH_TOKEN:
      return Object.assign({}, state, {
        token: action.token
      })
    default:
      return state
  }
}

export default combineReducers({
  token: getToken
})
