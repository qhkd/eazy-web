import * as Action from './actionTypes'

const login = (account) => {
  return {
    type: Action.LOGIN,
    account
  }
}

const logout = () => {
  return {
    type: Action.LOGOUT
  }
}

const register = (account) => {
  return {
    type: Action.REGISTER,
    account
  }
}

const receiveToken = (token) => {
  return {
    type: Action.RECEIVE_TOKEN,
    token
  }
}

const refreshToken = () => {
  return {
    type: Action.REFRESH_TOKEN,
    token: null
  }
}

export const goLogin = (account) => (dispatch) => {
  dispatch(login(account))
  const token = `Token_login_${account}`
  dispatch(receiveToken(token))
}

export const goLogout = () => (dispatch) => {
  dispatch(logout())
  dispatch(refreshToken())
}

export const goRegister = (account) => (dispatch) => {
  dispatch(register(account))
  const token = `Token_register_${account}`
  dispatch(receiveToken(token))
}
