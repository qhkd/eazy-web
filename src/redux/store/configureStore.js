import { createStore, applyMiddleware } from 'redux'
import dispatcher from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from 'redux/reducers/index'

const middleware = [dispatcher]
if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger())
}

const configureStore = (preloadedState) => {
  return createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(...middleware)
  )
}

export default configureStore
