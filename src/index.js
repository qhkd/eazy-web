import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createBrowserHistory } from 'history'
import configureStore from 'redux/store/configureStore'
import { I18nextProvider } from 'react-i18next'
import i18next from 'i18next'
import Backend from 'i18next-xhr-backend'

import App from 'containers/App'

const store = configureStore()
const browserHistory = createBrowserHistory()
i18next.use(Backend).init({
  lng: localStorage.getItem('lng'),

  ns: ['translations'],
  defaultNS: 'translations',

  react: {
    wait: true
  }
})

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <I18nextProvider i18n={i18next}>
        <Switch>
          <Route path='/' component={App} />
        </Switch>
      </I18nextProvider>
    </Router>
  </Provider>,
  document.getElementById('root')
)
